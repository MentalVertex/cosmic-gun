# Cosmic Gun
Entry for LÖVE Jam 2018

Download and play at the game's <a href="https://particularbicycle.itch.io/cosmic-gun">itch.io page</a>

![cg.webm](cg.webm)
![screenshot.png](screenshot.png)