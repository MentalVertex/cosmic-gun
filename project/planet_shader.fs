extern number sun_pos_x;
extern number sun_pos_y;
extern number planet_pos_x;
extern number planet_pos_y;
extern sampler2D night_tex;
extern number screen_scale;
extern number screen_translate_x;
extern number screen_translate_y;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
    vec4 c = Texel(texture, texture_coords);
    vec4 night_c = Texel(night_tex, texture_coords);
    
    vec2 resized_screen_coords = screen_coords;
    resized_screen_coords.x -= screen_translate_x;
    resized_screen_coords.y -= screen_translate_y;
    resized_screen_coords /= screen_scale;
    
    vec2 planet_pos = vec2(planet_pos_x, planet_pos_y);
    vec2 sun_pos = vec2(sun_pos_x, sun_pos_y);
    
    vec2 sun_to_planet = normalize(planet_pos - sun_pos);
    vec2 sun_to_tex = normalize(resized_screen_coords - sun_pos);
    
    float cross_prod = sun_to_tex.x * sun_to_planet.y - sun_to_tex.y * sun_to_planet.x;

    float coef = 4000*cross_prod;
    
    if (coef > 0.01){
        c = vec4(coef*c.r, coef*c.g, coef*c.b, c.a);
    } else {
        c = vec4(0.01*c.r, 0.01*c.g, 0.01*c.b, c.a) + night_c;
    }
    
    return c;
}