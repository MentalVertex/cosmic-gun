extern number sun_pos_x;
extern number sun_pos_y;
extern number comet_pos_x;
extern number comet_pos_y;
extern number proximity_coef;
extern number screen_scale;
extern number screen_translate_x;
extern number screen_translate_y;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
    vec4 c = Texel(texture, texture_coords);
    
    vec2 resized_screen_coords = screen_coords;
    resized_screen_coords.x -= screen_translate_x;
    resized_screen_coords.y -= screen_translate_y;
    resized_screen_coords /= screen_scale;
    
    vec2 comet_pos = vec2(comet_pos_x, comet_pos_y);
    vec2 sun_pos = vec2(sun_pos_x, sun_pos_y);
        
    vec2 sun_to_comet = normalize(comet_pos - sun_pos);
    vec2 sun_to_tex = normalize(resized_screen_coords - sun_pos);
    
    float cross_prod = sun_to_tex.x * sun_to_comet.y - sun_to_tex.y * sun_to_comet.x;

    float coef = 5000*cross_prod;
    coef = min(coef, 1.0);
    
    if (coef < 0.1){
        coef = 0.1f;
    }
    
    c = vec4(coef*c.r + 1.2*proximity_coef, coef*c.g + 0.8*proximity_coef, coef*c.b, c.a);
    
    return c;
}