Comet = {}
Comet.__index = Comet
function Comet:create(posx, posy, ang, r, rr, mfr)
   local o = {}
   setmetatable(o, Comet)
   o.pos_x = posx
   o.pos_y = posy
   o.angle = ang
   o.rot = r
   o.rot_rate = rr
   o.proximity = 0
   o.mark_for_remove = mfr
   return o
end


Shot = {}
Shot.__index = Shot
function Shot:create(posx, posy, ang)
    local o = {}
    setmetatable(o, Shot)
    o.pos_x = posx
    o.pos_y = posy
    o.angle = ang
    return o
end


CometHitAnim = {}
CometHitAnim.__index = CometHitAnim
function CometHitAnim:create(posx, posy, ang)
    local o = {}
    setmetatable(o, CometHitAnim)
    o.pos_x = posx
    o.pos_y = posy
    o.angle = ang
    o.idx = 0
    o.timer = 0
    o.mark_for_remove = false;
    return o
end


Score = {}
Score.__index = Score
function Score:Create(d, h, m)
    local o = {}
    setmetatable(o, Score)
    o.days = d
    o.hours = h
    o.minutes = m
    o.total_time = 0
    return o
end




function update_planet(dt)
    if enabled_controls == true then planet_rot = planet_rot + dt * 0.2 end
end


function draw_planet()
    if lost == false then
        planet_shader:send("sun_pos_x", sun_pos_x)
        planet_shader:send("sun_pos_y", sun_pos_y)
        planet_shader:send("screen_scale", resize_scale_x)
        planet_shader:send("screen_translate_x", resize_translate_x)
        planet_shader:send("screen_translate_y", resize_translate_y)
        love.graphics.setShader(planet_shader)
        love.graphics.draw(planet, planet_pos_x, planet_pos_y, planet_rot, 1, 1, planet_width / 2, planet_height / 2)
    else 
        love.graphics.draw(planet_explosion, planet_explosion_quad, planet_pos_x, planet_pos_y, planet_rot, 1, 1, planet_width / 2, planet_height / 2, 0, 0)
    end
end


function update_comets(dt)
    sec_counter = sec_counter + dt
    if sec_counter > 0.5 and enabled_controls == true then
        local side = math.random()
        if side < 0.25 then
            c = Comet:create(math.random()*win_x, 0, 0.78 + 1.57*math.random(), 0, 5*math.random()-2.5, false)
        elseif side < 0.5 then
            c = Comet:create(math.random()*win_x, win_y, -0.78 - 1.57*math.random(), 0, 5*math.random()-2.5, false)
        elseif side < 0.75 then
            c = Comet:create(0, math.random()*win_y, -0.78 + 1.57*math.random(), 0, 5*math.random()-2.5, false)
        else
            c = Comet:create(win_x, math.random()*win_y, -2.35 - 1.57*math.random(), 0, 5*math.random()-2.5, false)
        end
        table.insert(comets, c)
        comet_counter = comet_counter + 1
        sec_counter = 0
    end
    
    for i = #comets, 1, -1 do
        comets[i].pos_x = comets[i].pos_x + math.cos(comets[i].angle) * dt * 40
        comets[i].pos_y = comets[i].pos_y + math.sin(comets[i].angle) * dt * 40
        comets[i].rot = comets[i].rot + dt*comets[i].rot_rate
        local dis = math.sqrt((comets[i].pos_x-planet_pos_x)*(comets[i].pos_x-planet_pos_x)+(comets[i].pos_y-planet_pos_y)*(comets[i].pos_y-planet_pos_y))
        comets[i].proximity = dis
        if dis < 32 then
            table.remove(comets, i)
            lost = true
            register_score()
        end
    end
end


function draw_comets()
    love.graphics.setShader(comet_shader)
    
    comet_shader:send("sun_pos_x", sun_pos_x)
    comet_shader:send("sun_pos_y", sun_pos_y)
    comet_shader:send("screen_scale", resize_scale_x)
    comet_shader:send("screen_translate_x", resize_translate_x)
    comet_shader:send("screen_translate_y", resize_translate_y)
    
    for i=1, #comets do
        comet_shader:send("comet_pos_x", comets[i].pos_x)
        comet_shader:send("comet_pos_y", comets[i].pos_y)
        
        local proximity_coef = 0
        if lost == false then proximity_coef = 0.5 - 1*math.atan(1.0*comets[i].proximity-40)/3.1415 end
        comet_shader:send("proximity_coef", proximity_coef)
        love.graphics.draw(comet, comets[i].pos_x, comets[i].pos_y, comets[i].rot, 1, 1, comet:getWidth() / 2, comet:getHeight() / 2)
    end
end


function scroll_space(dt)
    local x, y, w, h = space_quad:getViewport()
    space_quad = love.graphics.newQuad(0, y + dt * 50, space:getWidth(), space:getWidth(), space:getWidth(), space:getHeight())
    if y > 1024 then
        space_quad = love.graphics.newQuad(0, 0, space:getWidth(), space:getWidth(), 1, 1)
    end
end


function draw_space()
    love.graphics.draw(space, space_quad, 0, 0, 0, 1, 1, 0, 0, 0, 0)
end


function update_ship(dt)
    ship_pos_x = ship_pos_x + ship_speed*dt*math.sin(ship_rot)
    ship_pos_y = ship_pos_y - ship_speed*dt*math.cos(ship_rot)
    
    ship_rot = ship_rot - ship_rot_speed*dt
    
    if lost == true then return end
    
    enabled_controls = true
    if ship_scale < 0.5 then
        ship_scale = ship_scale + 0.25 * dt
        enabled_controls = false
    end
    if enabled_controls == false then return end
    
    thrust_pressed = false
    if love.keyboard.isDown( "w" ) then
        thrust_sfx:play()
        thrust_pressed = true
        if ship_speed < 100 then ship_speed = ship_speed + 1 end
    else
        if ship_speed > 0 then ship_speed = ship_speed - 1 end
    end
    
    if love.keyboard.isDown( "a" ) then
        if ship_rot_speed < 3.1415 then ship_rot_speed = ship_rot_speed + 0.1 end
    elseif love.keyboard.isDown( "d" ) then
        if ship_rot_speed > -3.1415 then ship_rot_speed = ship_rot_speed - 0.1 end
    else
        ship_rot_speed = ship_rot_speed - 0.1*ship_rot_speed
    end
end


function draw_ship()
    comet_shader:send("comet_pos_x", ship_pos_x)
    comet_shader:send("comet_pos_y", ship_pos_y)
    love.graphics.draw(ship, ship_pos_x, ship_pos_y, ship_rot, ship_scale, ship_scale, ship:getWidth() / 2, ship:getHeight() / 2)
end


function love.keypressed( key, scancode, isrepeat )
    if state == 0 and key == "return" then
        state = 1
        return 
    end
    
    if lost == false and enabled_controls and key == "space" and shot_timeout > 0.1 then
        if ammo_count == 0 then
            empty_sfx:rewind(empty_sfx)
            empty_sfx:play()
        else
            shot_sfx:rewind(shot_sfx)
            shot_sfx:play()
            s = Shot:create(ship_pos_x + math.sin(ship_rot) * 20, ship_pos_y - math.cos(ship_rot) * 20, ship_rot)
            table.insert(shots, s)
            shot_timeout = 0
            ammo_count = ammo_count - 1
        end
    end
    
    if key == "m" then
        music_on = not music_on
    end
    
    if key == "n" then
        sfx_on = not sfx_on
    end
    
    if key == "r" then
        init_global_values(false)
        state = 0
    end
end


function love.keyreleased( key, scancode )
    if key == "w" then
        thrust_sfx:rewind(thrust_sfx)
        thrust_sfx:stop()
    end  
end


function update_shots(dt)
    shot_timeout = shot_timeout + dt
    
    for i = 1, #shots do
        shots[i].pos_x = shots[i].pos_x + math.sin(shots[i].angle) * dt * 200
        shots[i].pos_y = shots[i].pos_y - math.cos(shots[i].angle) * dt * 200
    end
    
    for i = #shots, 1, -1 do
        if shots[i].pos_x > win_x or shots[i].pos_y > win_y or shots[i].pos_x < 0 or shots[i].pos_y < 0 then
            table.remove(shots, i)
        end
    end
end


function draw_shots()
    for i = 1, #shots do
        love.graphics.draw(shot, shots[i].pos_x, shots[i].pos_y, shots[i].angle, 1, 1, shot:getWidth() / 2, shot:getHeight() / 2)
    end
end


function check_collisions()
    for i = #comets, 1, -1 do
        for j = #shots, 1, -1 do
            local dis_sq = (shots[j].pos_x-comets[i].pos_x)*(shots[j].pos_x-comets[i].pos_x) + (shots[j].pos_y-comets[i].pos_y)*(shots[j].pos_y-comets[i].pos_y)
            if dis_sq < 128 then
                --table.remove(shots, j)
                hit_sfx:rewind(hit_sfx)
                hit_sfx:play()
                comets[i].mark_for_remove = true
                cha = CometHitAnim:create(comets[i].pos_x, comets[i].pos_y, math.random()*3.14)
                table.insert(comet_hit_anims, cha)
            end
        end
    end
    
    for i = #comets, 1, -1 do
        if comets[i].mark_for_remove == true then
            table.remove(comets, i)
        end
    end
end


function update_ammo(dt)
    if lost == true then return end
    
    ammo_timer = ammo_timer + dt
    if ammo_timer > 15 and ammo_acquirable == false then
        ammo_pos_x = 32 + math.random() * (win_x-64)
        ammo_pos_y = 32 + math.random() * (win_y-64)
        ammo_acquirable = true
    end
    
    local dis_sq = (ship_pos_x - ammo_pos_x)*(ship_pos_x - ammo_pos_x) + (ship_pos_y - ammo_pos_y)*(ship_pos_y - ammo_pos_y)
    if dis_sq < 400 and ammo_acquirable == true then
        loaded_sfx:play()
        ammo_count = ammo_count + 50
        if ammo_count > 100 then ammo_count = 100 end
        ammo_acquirable = false
        ammo_timer = 0
    end
end


function draw_ammo_pack()
    if ammo_acquirable == true then
        love.graphics.draw(ammo_pack, ammo_pos_x, ammo_pos_y, 0, 1, 1, ammo_pack:getWidth()/2, ammo_pack:getHeight()/2)
    end
end


function draw_ammo_bar()
    local ammo_percent = ammo_count / 100
    ammo_bar_quad = love.graphics.newQuad(0, 0, 12+(ammo_bar:getWidth()-12)*ammo_percent, ammo_bar:getHeight(), ammo_bar:getWidth(), ammo_bar:getHeight())
    love.graphics.draw(ammo_bar, ammo_bar_quad, 0, 0, 0, 1, 1, 0, 0, 0, 0)
end


function thrust_anim(dt)
    thrust_anim_timer = thrust_anim_timer + dt
    if thrust_anim_timer > 0.1 then 
        thrust_anim_timer = 0
        thrust_anim_idx = thrust_anim_idx + 1
        if thrust_anim_idx == 5 then thrust_anim_idx = 0 end
    end
    thrust_quad = love.graphics.newQuad(16*thrust_anim_idx, 0, 16, thrust:getHeight(), thrust:getWidth(), thrust:getHeight())
end


function draw_thrust()
    if thrust_pressed == false or lost == true then return end
    local offset_x =  16 / 2
    local offset_y =  thrust:getHeight() / 2
    local thrust_pos_x = ship_pos_x - math.sin(ship_rot) * 22
    local thrust_pos_y = ship_pos_y + math.cos(ship_rot) * 22
    love.graphics.draw(thrust, thrust_quad, thrust_pos_x, thrust_pos_y, ship_rot, 0.3, 0.3, offset_x, offset_y, 0, 0)
end


function update_comet_hit_anims(dt)
    for i = 1, #comet_hit_anims, 1 do
        comet_hit_anims[i].timer = comet_hit_anims[i].timer + dt
        if comet_hit_anims[i].timer > 0.07 then
            comet_hit_anims[i].timer = 0
            comet_hit_anims[i].idx = comet_hit_anims[i].idx + 1
            if comet_hit_anims[i].idx == 5 then comet_hit_anims[i].mark_for_remove = true end
        end
    end
    
    for i = #comet_hit_anims, 1, -1 do
        if comet_hit_anims[i].mark_for_remove == true then
            table.remove(comet_hit_anims, i)
        end
    end
end


function draw_comet_hit_anims()
    local offset_x =  64 / 2
    local offset_y =  comet_hit:getHeight() / 2
    
    for i = 1, #comet_hit_anims, 1 do
        local quad = love.graphics.newQuad(64*comet_hit_anims[i].idx, 0, 64, comet_hit:getHeight(), comet_hit:getWidth(), comet_hit:getHeight())
        love.graphics.draw(comet_hit, quad, comet_hit_anims[i].pos_x, comet_hit_anims[i].pos_y, comet_hit_anims[i].angle, 1, 1, offset_x, offset_y, 0, 0)
    end
end


function update_planet_explosion_anim(dt)
    if ship_scale > 0 then ship_scale = ship_scale - 0.5*dt end

    planet_explosion_anim_timer = planet_explosion_anim_timer + dt
    if planet_explosion_anim_timer > 0.13 then
        planet_explosion_anim_timer = 0
        planet_explosion_anim_idx = planet_explosion_anim_idx + 1
        local width = planet_explosion:getWidth()
        local height = planet_explosion:getHeight()
        planet_explosion_quad = love.graphics.newQuad(64*planet_explosion_anim_idx, 0, 64, height, width, height)
    end
    
    if planet_explosion_anim_idx == 10 then
        end_round()
    end
end


function register_score()
    if current_score.total_time > best_score.total_time then
        best_score.days = current_score.days
        best_score.hours = current_score.hours
        best_score.minutes = current_score.minutes
        best_score.total_time = current_score.total_time
    end
end


function end_round()
    state = 0
    
    init_global_values(false)
end


function print_score_info()
    local day = "Day " .. tostring(current_score.days)
    local hrs = string.format('%02i', current_score.hours)
    local mins = string.format('%02i', current_score.minutes)
    love.graphics.print("Current", 460, 464)
    love.graphics.print(day, 460, 480)
    love.graphics.print(tostring(hrs) .. ":" .. tostring(mins), 460, 496)
    

    day = "Day " .. tostring(best_score.days)
    hrs = string.format('%02i', best_score.hours)
    mins = string.format('%02i', best_score.minutes)
    
    love.graphics.print("Best", 10, 464)
    
    if best_score.total_time > 1 then    
        love.graphics.print(day, 10, 480)
        love.graphics.print(tostring(hrs) .. ":" .. tostring(mins), 10, 496)
    else
        love.graphics.print("Day -", 10, 480)
        love.graphics.print("- " .. ":" .. " -", 10, 496)
    end
end


function update_score(dt)
    if lost == true then return end
    
    if planet_rot > 2*3.141592 then
        planet_rot = 0
        current_score.days = current_score.days + 1
    end
    
    local hrs = 24 * planet_rot / (2 * 3.1415)
    
    current_score.minutes = math.floor(60 * (hrs - math.floor(hrs)))
    current_score.hours = math.floor(hrs)
    
    current_score.total_time = 1440 * current_score.days + 60 * current_score.hours + current_score.minutes
end


function set_volumes(dt)
    if music_vol < 1.0 then
        music_vol = music_vol + 0.2*dt
    end
    if music_on == true then 
        music:setVolume(music_vol)
    else
        music:setVolume(0.0)
    end
    
    if sfx_on == true then
        shot_sfx:setVolume(0.2)
        empty_sfx:setVolume(0.5)
        thrust_sfx:setVolume(0.2)
        loaded_sfx:setVolume(1.0)
        hit_sfx:setVolume(1.0)
    else
        shot_sfx:setVolume(0.0)
        empty_sfx:setVolume(0.0)
        thrust_sfx:setVolume(0.0)
        loaded_sfx:setVolume(0.0)
        hit_sfx:setVolume(0.0)
    end
end


function game_loop(dt)
    set_volumes(dt)
    
    update_score(dt)
    
    update_planet(dt)
    scroll_space(dt)
    update_comets(dt)
    update_ship(dt)
    update_shots(dt)
    update_ammo(dt)
    thrust_anim(dt)
    update_comet_hit_anims(dt)
    
    if lost == true then update_planet_explosion_anim(dt) end
    
    check_collisions()
    
    timer = timer + dt
    sun_pos_x = win_x / 2 + math.cos(0.01*timer) * 100*win_x / 2
    sun_pos_y = win_y / 2 + math.sin(0.01*timer) * 100*win_y / 2 
    
    if love.keyboard.isDown( "escape" ) then
        love.event.quit()
    end
end


function game_draw()
    draw_space()
    draw_planet()
    draw_comets()
        
    love.graphics.setShader()
    
    draw_shots()
    draw_ammo_pack()
    draw_ship()
    draw_thrust()
    draw_comet_hit_anims()
    draw_ammo_bar()    
    
    print_score_info()
end


function intro_loop(dt)
    set_volumes(dt)
    
    if love.keyboard.isDown( "escape" ) then
        love.event.quit()
    end
end


function intro_draw()
    love.graphics.draw(intro, 0, 0)
end


function love.update(dt)
    if state == 0 then
        intro_loop(dt)
    else
        game_loop(dt)
    end
end


function love.draw()
    love.graphics.origin()
    
    love.graphics.translate(resize_translate_x, resize_translate_y)
    love.graphics.scale(resize_scale_x, resize_scale_y)
    
    if state == 0 then
        intro_draw()
    else
        game_draw()
    end
end


function love.resize(w, h)
    if w < h then
        resize_scale_x = w / win_x
        resize_scale_y = w / win_y
        resize_translate_x = 0
        resize_translate_y = (h - w) / 2
    else 
        resize_scale_x = h / win_x
        resize_scale_y = h / win_y
        resize_translate_x = (w - h) / 2
        resize_translate_y = 0
    end
end


function init_global_values(first_time)
    math.randomseed(os.time())
        
    sun_pos_x = 256
    sun_pos_y = 0
    
    current_score = Score:Create(1, 0, 0)
    if first_time == true then
        best_score = Score:Create(1, 0, 0)
    end
    
    state = 0
    timer = 0
    
    thrust_quad = love.graphics.newQuad(0, 0, 1, 1, 1, 1)
    thrust_anim_idx = 0
    thrust_anim_timer = 0
    
    lost = false
    planet_explosion_quad = love.graphics.newQuad(0, 0, 1, 1, 1, 1)
    planet_explosion_anim_idx = 0
    planet_explosion_anim_timer = 0
    
    ammo_bar_quad = love.graphics.newQuad(0, 0, 1, 1, 1, 1)
    ammo_timer = 0
    ammo_pos_x = 0
    ammo_pos_y = 0
    ammo_count = 100
    ammo_acquirable = false
    
    shot_timeout = 0
    
    if first_time == true then music_vol = 0 music:setVolume(0.0) end
    if first_time == true then music_on = true end
    music:setLooping(true)
    music:play()
    if first_time == true then sfx_on = true end
    
    planet_pos_x = win_x / 2
    planet_pos_y = win_y / 2
    planet_rot  = 0.0
    planet_width = planet:getWidth()
    planet_height = planet:getHeight()
    
    ship_pos_x = win_x / 2
    ship_pos_y = win_y / 2
    ship_rot = 0
    ship_speed = 0
    ship_rot_speed = 0
    ship_scale = 0
    
    thrust_pressed = false
    
    enabled_controls = false
    
    space_quad = love.graphics.newQuad(0, 0, space:getWidth(), space:getWidth(), space:getWidth(), space:getHeight())
    
    sec_counter = 0
    comet_counter = 0
    
    comets = {}
    shots = {}
    comet_hit_anims = {}
    
    planet_shader:send("night_tex", planet_night);
    planet_shader:send("planet_pos_x", win_x / 2)
    planet_shader:send("planet_pos_y", win_y / 2)
end


function love.load()  
    win_x = 512
    win_y = 512
    resize_scale_x = 1
    resize_scale_y = 1
    resize_translate_x = 0
    resize_translate_y = 0
    
    love.window.setTitle("Cosmic Gun")
    success = love.window.setMode(win_x, win_y, {resizable = true})
    
    ico = love.graphics.newImage("assets/img/solar_gun_ico.png");
    love.window.setIcon(ico:getData())
    
    music = love.audio.newSource("assets/audio/cosmic_gun.mp3", "static")
    
    shot_sfx = love.audio.newSource("assets/audio/shot.wav", "static")
    empty_sfx = love.audio.newSource("assets/audio/empty.wav", "static")
    thrust_sfx = love.audio.newSource("assets/audio/thrust.wav", "static")
    loaded_sfx = love.audio.newSource("assets/audio/loaded.wav", "static")
    hit_sfx = love.audio.newSource("assets/audio/hit.wav", "static")
    
    intro = love.graphics.newImage("assets/img/intro.png")
    planet = love.graphics.newImage("assets/img/planet.png")
    sun = love.graphics.newImage("assets/img/sun.png")
    planet_night = love.graphics.newImage("assets/img/planet_night.png")
    comet = love.graphics.newImage("assets/img/comet.png")
    space = love.graphics.newImage("assets/img/space.png")
    ship = love.graphics.newImage("assets/img/ship.png")
    shot = love.graphics.newImage("assets/img/shot.png")
    ammo_pack = love.graphics.newImage("assets/img/ammo_package.png")
    ammo_bar = love.graphics.newImage("assets/img/ammo_bar.png")
    thrust = love.graphics.newImage("assets/img/thrust.png")
    comet_hit = love.graphics.newImage("assets/img/comet_hit_anim.png")
    planet_explosion = love.graphics.newImage("assets/img/planet_explosion.png")
    
    planet_shader = love.graphics.newShader("planet_shader.fs")
    comet_shader = love.graphics.newShader("comet_shader.fs")
    
    init_global_values(true)
end